import java.io.*;
import java.util.*;

public class IplProject {

    static String path = "/home/sourav/IdeaProjects/IplProject/src/matches.csv";
    static String line = "";


    public static List<MatchesData> getMatches() {
        List<MatchesData> matches = new ArrayList<>();
        int skipFirstLine = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            while ((line = br.readLine()) != null) {

                if (skipFirstLine == 0) {
                    skipFirstLine++;
                    continue;
                }
                MatchesData objMatch = new MatchesData();
                String[] array = line.split(",");

                objMatch.setId(Integer.parseInt(array[0]));
                objMatch.setSeason(array[1]);
                objMatch.setCity(array[2]);
                objMatch.setDate(array[3]);
                objMatch.setTeam1(array[4]);
                objMatch.setTeam2(array[5]);
                objMatch.setTossWinner(array[6]);
                objMatch.setTossDecision(array[7]);
                objMatch.setResult(array[8]);
                objMatch.setDl(Integer.parseInt(array[9]));
                objMatch.setWinner(array[10]);
                objMatch.setWinByRuns(Integer.parseInt(array[11]));
                objMatch.setWinByWickets(Integer.parseInt(array[12]));
                objMatch.setPlayerOfMatch(array[13]);
                objMatch.setVenue(array[14]);

                matches.add(objMatch);

            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return matches;
    }

    public static List<DeliveriesData> getDeliveries() {

        List<DeliveriesData> deliveries = new ArrayList<>();
        int skipFirstLine = 0;
        path = "/home/sourav/IdeaProjects/IplProject/src/deliveries.csv";

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            while ((line = br.readLine()) != null) {
                if (skipFirstLine == 0) {
                    skipFirstLine++;
                    continue;
                }
                DeliveriesData objDelivery = new DeliveriesData();
                String[] array = line.split(",");

                objDelivery.setMatchId(Integer.parseInt(array[0]));
                objDelivery.setInnings(Integer.parseInt(array[1]));
                objDelivery.setBattingTeam(array[2]);
                objDelivery.setBowlingTeam(array[3]);
                objDelivery.setOver(Integer.parseInt(array[4]));
                objDelivery.setBowl(Integer.parseInt(array[5]));
                objDelivery.setBatsman(array[6]);
                objDelivery.setNonStriker(array[7]);
                objDelivery.setBowler(array[8]);
                objDelivery.setIsSuperOver(Integer.parseInt(array[9]));
                objDelivery.setWideRuns(Integer.parseInt(array[10]));
                objDelivery.setByeRuns(Integer.parseInt(array[11]));
                objDelivery.setLegByeRuns(Integer.parseInt(array[12]));
                objDelivery.setNoBallRuns(Integer.parseInt(array[13]));
                objDelivery.setPenaltyRuns(Integer.parseInt(array[14]));
                objDelivery.setBatsmanRuns(Integer.parseInt(array[15]));
                objDelivery.setExtraRuns(Integer.parseInt(array[16]));
                objDelivery.setTotalRuns(Integer.parseInt(array[17]));
//               objDelivery.setPlayerDismissed(array[18]);
//               objDelivery.setDismissalKind(array[19]);
//               objDelivery.setFielder(array[20]);

                deliveries.add(objDelivery);

            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return deliveries;
    }

    //Problem1
    public static Map matchesPlayedPerYear(List<MatchesData> data) {

        Map<String, Integer> matchesPlayedPerYear = new HashMap<>();

        String year = data.get(0).getSeason();
//       System.out.println(year);
        int counter = 1;
        for (MatchesData element : data) {
            String yearElement = element.getSeason();
            if (yearElement.equals(year)) {
                counter++;
            } else {
                matchesPlayedPerYear.put(year, counter);
                year = yearElement;
                counter = 1;
            }

        }
        matchesPlayedPerYear.put(year, counter);
        return matchesPlayedPerYear;
    }

    //Problem2
    public static Map numberOfMatchesWon(List<MatchesData> data) {

        Map<String, Integer> winners = new HashMap<>();
        ArrayList<String> winner = new ArrayList<>();

        for (MatchesData element : data) {

            winner.add(element.getWinner());
        }
        int counter, l = 15;
        for (int index = 0; index < winner.size(); index++) {
            String name = winner.get(index);
            counter = 1;
//            System.out.print(name+" ");
            for (int index2 = index + 1; index2 < winner.size(); index2++) {
//                System.out.print(winner.get(index2)+" ");
                if ((winner.get(index2)).equalsIgnoreCase(name)) {
//                    System.out.println("match" + index +" " +index2+" "+name);
                    counter++;
                    winner.remove(index2);
                    index2--;
                }
//                System.out.println();
            }
            if (name.equals("")) {
                name = "No Result";
            }
            winners.put(name, counter);
        }
        return winners;
//
    }

    //Problem3
    public static Map extraRunsConceeded(List<MatchesData> matchData, List<DeliveriesData> deliveryData, String year) {

        ArrayList<Integer> matchId = new ArrayList<>();

        //Getting the matchID of every match played in 2016
        for (MatchesData element : matchData) {
            if (element.getSeason().equals(year)) {
                matchId.add(element.getId());
            }
        }

        //Getting the data of only year 2016
        List<DeliveriesData> deliveriesData2016 = new ArrayList<>();
        for (DeliveriesData elment : deliveryData) {
            if (elment.getMatchId() >= matchId.get(0) && elment.getMatchId() <= matchId.get(matchId.size() - 1)) {
                deliveriesData2016.add(elment);
            }
        }

        Map<String, Integer> extraRunsConceded = new HashMap<>();
        for (int index = 0; index < deliveriesData2016.size(); index++) {

            String bowlingTeam = deliveriesData2016.get(index).getBowlingTeam();
            int extraRuns = deliveriesData2016.get(index).getExtraRuns();

            for (int index2 = index + 1; index2 < deliveriesData2016.size(); index2++) {
                if (deliveriesData2016.get(index2).getBowlingTeam().equals(bowlingTeam)) {
                    extraRuns += deliveriesData2016.get(index2).getExtraRuns();
                    deliveriesData2016.remove(index2);
                    index2--;
                }
            }

            extraRunsConceded.put(bowlingTeam, extraRuns);
        }

        return extraRunsConceded;
    }

    //Problem4
    public static Map<Float, String > topEconomicalBowlers(List<MatchesData> matchData, List<DeliveriesData> deliveryData, String year) {

        ArrayList<Integer> matchId = new ArrayList<>();

        //Getting the matchID of every match played in a specific year
        for (MatchesData element : matchData) {
            if (element.getSeason().equals(year)) {
                matchId.add(element.getId());
            }
        }

        List<DeliveriesData> deliveriesDataPerYear = new ArrayList<>();
        for (DeliveriesData elment : deliveryData) {
            if (elment.getMatchId() >= matchId.get(0) && elment.getMatchId() <= matchId.get(matchId.size() - 1)) {
                deliveriesDataPerYear.add(elment); // Getting deliveries data for 2015 only
            }
        }

        Map<Float, String> bowlerEconomy = new TreeMap<>();
        float economy = 0.0F;
        for (int index = 0; index < deliveriesDataPerYear.size(); index++) {

            String bowler = deliveriesDataPerYear.get(index).getBowler();
            float balls = 1.0F;
            float runs = (deliveriesDataPerYear.get(index).getTotalRuns() - (deliveriesDataPerYear.get(index).getByeRuns() + deliveriesDataPerYear.get(index).getLegByeRuns())) * 1.0F;
            for (int index2 = index + 1; index2 < deliveriesDataPerYear.size(); index2++) {

                if (deliveriesDataPerYear.get(index2).getBowler().equals(bowler)) {
                    if (deliveriesDataPerYear.get(index2).getBowl() <= 6) {
                        balls++;
                    }//counting the total number of balls bowled by the bowler

                    //adding the total runs conceded excluding bye runs
                    runs += (deliveriesDataPerYear.get(index2).getTotalRuns() - (deliveriesDataPerYear.get(index2).getByeRuns() + deliveriesDataPerYear.get(index2).getLegByeRuns())) * 1.0F;
                    deliveriesDataPerYear.remove(index2);
                    index2--;
                }
            }
            economy = (runs / balls) * 6.0F;
            bowlerEconomy.put(economy, bowler); //adding the economy rate and bowler name to the treeMap which automatically sorts it
        }
        return bowlerEconomy;
    }

    //Problem5
    public static Map highestBattingStrikeRatePerSeason(List<MatchesData> matchData, List<DeliveriesData> deliveryData) {

        Set<String> season = new HashSet<>();
        for (MatchesData element : matchData) {
            season.add(element.getSeason()); //Storing all the seasons in a set to remove all duplicates
        }

        Map<String, Map> highestSeasonStrikeRate = new TreeMap<>();
        for (String elment : season) {

            String year = elment;
            ArrayList<Integer> matchId = new ArrayList<>();

//            For every season getting the matchId's of only that season
            for (MatchesData element : matchData) {
                if (element.getSeason().equals(year)) {
                    matchId.add(element.getId());
                }
            }

//            Getting DeliveriesData of each particular season
            List<DeliveriesData> deliveriesDataPerSeason = new ArrayList<>();
            for (DeliveriesData element2 : deliveryData) {
                if (element2.getMatchId() >= matchId.get(0) && element2.getMatchId() <= matchId.get(matchId.size() - 1)) {
                    deliveriesDataPerSeason.add(element2);
                }
            }

            float highestStrikeRate = 0.0F;

            Map<String, Float> batsmanStrikeRate = new HashMap<>();
            batsmanStrikeRate.put(deliveriesDataPerSeason.get(0).getBatsman(), 0.0F);
//            Getting batting strike rate of every batsman each season
            for (int index = 0; index < deliveriesDataPerSeason.size(); index++) {

                String batsman = deliveriesDataPerSeason.get(index).getBatsman();
                float balls = 1.0F, runs = deliveriesDataPerSeason.get(index).getBatsmanRuns();

                for (int index2 = index + 1; index2 < deliveriesDataPerSeason.size(); index2++) {

                    if (deliveriesDataPerSeason.get(index2).getBatsman().equals(batsman)) {
                        runs += deliveriesDataPerSeason.get(index2).getBatsmanRuns();

                        if (deliveriesDataPerSeason.get(index2).getBowl() <= 6) {
                            balls++;
                        }

                        deliveriesDataPerSeason.remove(index2);
                        index2--;
                    }
                }
                float strikeRate = (runs / balls) * 100.0F;
                if (strikeRate > highestStrikeRate) {
                    batsmanStrikeRate.replace(batsman, strikeRate);
                    highestStrikeRate = strikeRate;
                }

            }
//            System.out.println(batsmanStrikeRate+" "+year);
            highestSeasonStrikeRate.put(year, batsmanStrikeRate);
        }
        return highestSeasonStrikeRate;
    }

    //Problem 6. Teams who won toss as well as match

    public static List<String> wonTossAndMatch(List<MatchesData> matchesData) {

        List<String> wonTossAndMatch = new ArrayList<>();

        for (MatchesData element : matchesData)  {
            String tossWinner = element.getTossWinner();
            String winner = element.getWinner();

            if(tossWinner.equals(winner)) {
                wonTossAndMatch.add(winner);
            }
        }
        return wonTossAndMatch;
    }


    //Diplay the venue where Sunrises hyderebad won the maximum matches
    public static Map maximumWinVenue(List<MatchesData> matchesData) {

        int maximumWinVenue = 0;
        List<String> sunrisesWinVenues = new ArrayList<>();
        for(MatchesData element : matchesData) {
            String winner = element.getWinner();

            if(winner.equals("Sunrisers Hyderabad")) {

                sunrisesWinVenues.add(element.getVenue());
            }
        }
        Map<Integer, String> maximumWinVenueName = new HashMap<>();
        for(int index = 0;index<sunrisesWinVenues.size();index++) {
            String venue = sunrisesWinVenues.get(index);
            int flag = 1;
            for(int index2 = index+1;index2<sunrisesWinVenues.size();index2++) {
                if(sunrisesWinVenues.get(index2).equals(venue)) {
                    flag++;
                    sunrisesWinVenues.remove(index2);
                    index2--;
                }
            }
            if (flag > maximumWinVenue) {
                maximumWinVenue = flag;
                maximumWinVenueName.put(maximumWinVenue,venue);
            }
        }
        return maximumWinVenueName;

    }


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        List<MatchesData> matchesData = new ArrayList<>();
        matchesData.addAll(getMatches());
        List<DeliveriesData> deliveryData = new ArrayList<>();
        deliveryData.addAll(getDeliveries());

        System.out.println("1. Number of matches played per year of all the years in IPL.");
        System.out.println(matchesPlayedPerYear(matchesData));
        System.out.println("2. Number of matches won of all teams over all the years of IPL.");
        System.out.println(numberOfMatchesWon(matchesData));
        System.out.println("Enter a year");
        String year = sc.nextLine();
        System.out.format("3. For the year %s get the extra runs conceded per team.\n",year);
        System.out.println(extraRunsConceeded(matchesData, deliveryData, year));
        System.out.println("Enter a year");
        String year2 = sc.nextLine();
        System.out.format("4. For the year %s the top economical bowlers-\n",year2);
        System.out.println(topEconomicalBowlers(matchesData, deliveryData, year2));
        System.out.println("5. Custom question, highest batting strike rate per season");
        System.out.println(highestBattingStrikeRatePerSeason(matchesData, deliveryData));

        System.out.println("6. Teams which won toss and match: ");
        System.out.println(wonTossAndMatch(matchesData));
        System.out.println("7. Stadium at which Sunrises Hyderabad won the most matches over all seasons of IPL");
        System.out.println(maximumWinVenue(matchesData));
    }
}